from django.conf.urls import url

from . import views
from . import views_message
from . import views_mindmap
from . import views_summary
from . import views_checkMeeting
from . import views_users

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^message', views_message.message, name='message'),
    url(r'^summary', views_summary.summary, name='summary'),
    url(r'^mindmap', views_mindmap.mindmap, name='mindmap'),
    url(r'^checkMeeting', views_checkMeeting.checkMeeting, name='checkMeeting'),
    url(r'^users', views_users.users, name='users'),
]
