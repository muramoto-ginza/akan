import json
import datetime

from django.http import JsonResponse
from django.views import generic
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .models import Keyword

@csrf_exempt
def keyword(request):
    # return HttpResponse(request.POST.get('name'))

    # if request.is_ajax():
    keyword_id = request.POST.getlist('data[keyword_id]')[0]
    speech_id = request.POST.getlist('data[speech_id]')[0]
    keyword = request.POST.getlist('data[keyword]')[0]

    # Insert
    k = Keyword(keyword_id=keyword_id, message=message, keyword=keyword).save()

    # SELECT
    keywordList = Keyword.objects.all().order_by('speech_id')[:20].reverse()
    keyword_serialized = serializers.serialize('json', keywordList)
    return JsonResponse(keyword_serialized, safe=False)
