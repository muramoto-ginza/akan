import json
import datetime

from django.http import JsonResponse
from django.views import generic
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .models import User

@csrf_exempt
def users(request):
    userList = User.objects.all().order_by('user_id')
    user_serialized = serializers.serialize('json', userList)
    return JsonResponse(user_serialized, safe=False)


