# coding:utf-8
import json
import datetime
import httplib, urllib, base64

from django.http import JsonResponse
from django.views import generic
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .models import Mindmap

@csrf_exempt
def mindmap(request):

    #SELECT
    #mindmap = Mindmap.objects.all().order_by('mindmap_id')[:20].reverse()
    mindmap = Mindmap.objects.all().order_by('mindmap_id')

    rtn = u'<ul><li><a href="#">働き方改革</a><ul>'
    for data in mindmap:
        if data.parent_id == '0':
            rtn = rtn + '<li><a href="#" target="_blank">' + data.child_text + '</a>'
            for data2 in mindmap:
                if data.child_id == data2.parent_id:
                    rtn = rtn + '<ul><li><a href="#">' + data2.child_text + '</a></li></ul>'
            rtn = rtn + '</li>'
    rtn = rtn + '</li></ul>'
    pprint(rtn)
    return HttpResponse(rtn)
    #maindmap_serialized = serializers.serialize('json', rtn)
    #return JsonResponse(mindmap_serialized, safe=False)