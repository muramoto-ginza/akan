import json
import datetime

from django.http import JsonResponse
from django.views import generic
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .models import User


class UserList(generic.ListView):
    model = User

@csrf_exempt
def index(request):
    #return HttpResponse(request.POST.get('name'))
    #if request.is_ajax():
    #print(request.POST.getlist())
    user_id = request.POST.getlist('data[user_id]')[0]
    name = request.POST.getlist('data[name]')[0]
    status = 0
    px = request.POST.getlist('data[pX]')[0]
    py = request.POST.getlist('data[pY]')[0]
    pz = request.POST.getlist('data[pZ]')[0]
    rx = request.POST.getlist('data[rX]')[0]
    ry = request.POST.getlist('data[rY]')[0]
    rz = request.POST.getlist('data[rZ]')[0]
    lasttime = datetime.datetime.today()

    print(user_id)
    #Insert or Update
    User.objects.update_or_create(
        user_id=user_id,

    )

    #Update
    #user = User.objects.filter(user_id=user_id).first()
    user = User.objects.get(user_id=user_id)
    user.user_id = user_id
    user.name = name
    user.status = status
    user.px = px
    user.py = py
    user.pz = pz
    user.rx = rx
    user.ry = ry
    user.rz = rz
    user.lasttime = lasttime
    user.save()

    #SELECT
    userList = User.objects.all().order_by('user_id')[:20].reverse()
    user_serialized = serializers.serialize('json', userList)
    return JsonResponse(user_serialized, safe=False)


