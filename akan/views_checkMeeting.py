import json
import datetime

from django.http import JsonResponse
from django.views import generic
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .models import User

@csrf_exempt
def checkMeeting(request):

    user_id = request.POST.getlist('data[user_id]')[0]
    # SELECT
    userList = User.objects.all().filter(user_id=user_id).order_by('user_id')[:20].reverse()
    user_serialized = serializers.serialize('json', userList)
    return JsonResponse(user_serialized, safe=False)