# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
#import datetime
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.
class User(models.Model):
    #user_id = models.AutoField(primary_key=True)
    user_id = models.CharField(max_length=200, primary_key=True)
    name = models.CharField(null=True, max_length=200)
    status = models.IntegerField(null=True, default=1)
    meeting_status = models.IntegerField(null=True, default=0)
    px = models.CharField(null=True, default=0, max_length=20)
    py = models.CharField(null=True, default=0, max_length=20)
    pz = models.CharField(null=True, default=0, max_length=20)
    rx = models.CharField(null=True, default=0, max_length=20)
    ry = models.CharField(null=True, default=0, max_length=20)
    rz = models.CharField(null=True, default=0, max_length=20)
    #lasttime = models.DateTimeField(default=datetime.datetime.now())
    lasttime = models.DateTimeField(null=True)
    #lasttime =models.CharField(default=0, max_length=20)

    def __str__(self):
        #return self.user_id, self.name, self.status, self.meeting_status, self.px, self.py, self.pz, self.rx, self.ry, self.rz, self.lasttime
        return 'user_id={0}, name={1}, status={2}, meeting_status={3}, px={4}, py={5}, pz={6}, rx={7}, ry={8}, rz={9}, lasttime={10}'.format(self.user_id, self.name, self.status, self.px, self.py, self.pz, self.rx, self.ry, self.rz, self.lasttime)
        #return u'%s %s %s %s %s %s %s %s %s %s %s ' % (self.user_id, self.name, self.status, self.meeting_status, self.px, self.py, self.pz, self.rx, self.ry, self.rz, self.lasttime)

class Message(models.Model):
    #speech_id = models.AutoField(primary_key=True)
    #user =  models.ForeignKey(User)
    speech_id = models.AutoField(primary_key=True)
    name = models.CharField(default="", max_length=200)
    message = models.CharField(max_length=2048)
    datetime = models.DateTimeField(null=True)
    summary_flg = models.BooleanField(default=0)

    def __str__(self):
        #return self.speech_id, self.user, self.message, self.datetime
        return 'name={0}, message={1}, datetime={2}, summary_flg={3}'.format(
            self.name, self.message, self.datetime, self.summary_flg)

class Summary(models.Model):
    summary_id = models.AutoField(primary_key=True)
    #speech =  models.ForeignKey(Message)
    summary = models.CharField(max_length=1024)

    def __str__(self):
        #return self.summarize_id, self.user, self.speech, self.summarize
        #return 'summary_id={0}, speech={1}, summary={2}'.format(
        #    self.summary_id, self.speech, self.summary)
        return 'summary_id={0}, summary={1}'.format(
            self.summary_id, self.summary)

class Keyword(models.Model):
    #keyword_id = models.AutoField(primary_key=True)
    keyword_id = models.IntegerField(primary_key=True)
    speech = models.ForeignKey(Message)
    keyword = models.CharField(max_length=20)

    def __str__(self):
        #return self.keyword_id, self.speech, self.keyword
        return 'keyword_id={0}, speech={1}, keyword={2}'.format(
            self.keyword_id, self.speech, self.keyword)


class Mindmap(models.Model):
    mindmap_id = models.AutoField(primary_key=True)
    #central_text = models.CharField(default="", max_length=200)
    parent_id = models.CharField(default="", max_length=200)
    parent_text = models.CharField(default="", max_length=200)
    child_id = models.CharField(default="", max_length=200)
    child_text = models.CharField(default="", max_length=200)

    def __str__(self):
        return 'mindmap_id={0}, parent_id={1}, parent_text={2}, child_id={3}, child_text={4}'.format(
            self.mindmap_id, self.parent_id, self.parent_text, self.child_id, self.child_text)
