# coding:utf-8
import json
import datetime
import httplib, urllib2, base64, urllib
import gensim
import MeCab

from django.http import JsonResponse
from django.views import generic
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .models import Message
from .models import Summary
from .models import Mindmap



@csrf_exempt
def message(request):
    name = request.POST.getlist('data[name]')[0]
    message = request.POST.getlist('data[message]')[0]
    lasttime = datetime.datetime.today()
    msg = Message(message=message, name=name, datetime=lasttime).save()

    messageList = Message.objects.filter(summary_flg=0)[:20]
    # サマリしていない発言が20個以上の場合
    if(len(messageList) >= 20):
        # summary_flg = 1
        for message in messageList:
            Message.objects.update_or_create(
                speech_id=message.speech_id,
            )
            message.summary_flg = 1
            message.save()
        word = ""
        for message in messageList:
            word += message.message
            word += u"。"

        # リクルートAPIで３つの発言に要約
        conn = httplib.HTTPConnection("localhost:8080")
        conn.request("GET", "/summarize?sent_limit=3&text=" + urllib2.quote(word.encode("utf-8")))
        response = conn.getresponse()
        data = response.read()
        ary = json.loads(data)
        Summary(summary=ary["summary"][0]).save()
        Summary(summary=ary["summary"][1]).save()
        Summary(summary=ary["summary"][2]).save()
        # MicrosoftのAPIでtopic抽出
        TOPWORD = u'働き'
        topics = get_topic(word)
        wikipath = "/home/ec2-user/word2vec.gensim.model"
        model = gensim.models.Word2Vec.load(wikipath)
        mindmapList = Mindmap.objects.all()
        kyori_id = []
        kyori_word = []
        kyori = []
        for topic in topics:
            try:
                model.wv[topic]
            except Exception as e:
                continue
            kyori_id.append("0")
            kyori_word.append(u'働き')
            kyori.append(model.wv.similarity(u'働き', topic))
            insflag = True
            for mml in mindmapList:
                if mml.parent_text == topic:
                    insflag = False
                    break
                if mml.child_text == topic:
                    insflag = False
                    break
                # 親との距離
                if mml.parent_id != "0":
                    kyori_id.append(mml.parent_id)
                    kyori_word.append(mml.parent_text)
                    kyori.append(model.wv.similarity(mml.parent_text, topic))
                # 子の距離
                kyori_id.append(mml.child_id)
                kyori_word.append(mml.child_text)
                kyori.append(model.wv.similarity(mml.child_text, topic))
            # 新語は登録
            if insflag:
                mizikaiKyori = 0
                mizikaiIndex = 0
                roopIndex = 0
                for tmpKyori in kyori:
                    if mizikaiKyori < tmpKyori:
                        mizikaiKyori = tmpKyori
                        mizikaiIndex = roopIndex
                    roopIndex += 1
                Mindmap(parent_id=kyori_id[mizikaiIndex], parent_text=kyori_word[mizikaiIndex], child_id="Child"+str(len(kyori_id)), child_text=topic).save()
    return HttpResponse("Hello, World")


# 3つのトピックを取得
def get_topic(word):
    m = MeCab.Tagger("-Ochasen ")
    headers = {
        # Request headers
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': 'c89271e3c70a40a3ac1c6dae1e5fab50',
    }
    params = urllib.urlencode({})
    body = {
        "documents": [
            {
                "language": "ja",
                "id": "1",
                "text": word
            }
        ]
    }
    word = []

    try:
        conn = httplib.HTTPSConnection('westus.api.cognitive.microsoft.com')
        conn.request("POST", "/text/analytics/v2.0/keyPhrases?%s" % params, json.dumps(body), headers)
        response = conn.getresponse()
        data = response.read()
        ary = json.loads(data)
        ary_len = 5
        cnt = 0
        for tmp in ary["documents"][0]["keyPhrases"]:
            moji = tmp.encode('utf-8')
            node = m.parseToNode(moji)
            while node:
                hinshi = node.feature.split(',')[0]
                if hinshi == '名詞':
                    word.append(tmp)
                    cnt += 1
                node = node.next
                if cnt == ary_len:
                    break
            if cnt == ary_len:
                break
        conn.close()
    except Exception as e:
        print(e.message)
    return word
