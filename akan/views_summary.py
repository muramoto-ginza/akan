# coding:utf-8
import json
import datetime
import httplib, urllib, base64

from django.http import JsonResponse
from django.views import generic
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .models import Summary

@csrf_exempt
def summary(request):

    # SELECT
    summary = Summary.objects.all().order_by('summary_id')

    #Return
    rtn = u"<ul>"
    for data in summary:
        rtn = rtn + '<li>' + data.summary + '</li>'

    rtn = rtn + '</ul>'
    return HttpResponse(rtn)
    #summary_serialized = serializers.serialize('json', summary)
    #return JsonResponse(summary_serialized, safe=False)
